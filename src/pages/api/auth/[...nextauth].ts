import NextAuth from 'next-auth/next'
import CredentialsProvider from 'next-auth/providers/credentials'

import { login } from '../../../services/vivatranslate'

export default NextAuth({
  providers: [
    CredentialsProvider({
      id: 'credentials',
      name: 'credentials',
      credentials: {
        email: { label: 'Email', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        try {
          const user = await login({
            email: credentials.email,
            password: credentials.password,
          })

          if (user.data && user.data.token) {
            return user.data
          }
        } catch (e) {
          throw new Error(e)
        }
      },
    }),
  ],
  callbacks: {
    jwt: async ({ token, user }) => {
      if (user) {
        token.accessToken = user.token
        token.email = user.user.email
      }
      return Promise.resolve(token)
    },
    session: async ({ session, token }) => {
      session.accessToken = token.accessToken as string
      return Promise.resolve(session)
    },
  },
  pages: {
    signIn: '/login',
  },
})
