export const types = {
  SET_QUESTIONARY: '[Quiz] Set Questionary',
  SET_MY_CHOICE: '[Quiz] Set My Choice',
  RESET_QUIZ: '[Quiz] Reset Quiz',
  SET_MY_QUESTIONARIES: '[Quiz] Set My Questionaries',
}
