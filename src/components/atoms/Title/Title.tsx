import { FC } from 'react'

import { TitleProps } from './interface'
import styles from './Title.module.scss'

const Title: FC<TitleProps> = ({
  children,
  size = 'md',
  className = '',
  as,
}) => {
  const Htag = as

  return (
    <Htag className={`${styles.title} ${styles[size]} ${className}`}>
      {children}
    </Htag>
  )
}

export default Title
