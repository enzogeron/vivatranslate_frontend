import { FC } from 'react'

import { ContainerProps } from './interface'
import styles from './Container.module.scss'

const Container: FC<ContainerProps> = ({ children, className = '', side }) => {
  return (
    <div className={`${styles.container} ${styles[side]} ${className}`}>
      {children}
    </div>
  )
}

export default Container
